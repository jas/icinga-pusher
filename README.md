Passive Icinga Checks: icinga-pusher
------------------------------------

The `icinga-pusher` script is used on a client machine to invoke a
Nagios/Icinga test plugin locally and submit the result passively to a
central Icinga server.

The script requires a configuration file `/etc/default/icinga-pusher`
with some information about the server setup.  For example:

```
ICINGA_PUSHER_CREDS="-u pusher:blahonga"
ICINGA_PUSHER_URL="https://icinga.yoursite.com:5665"
ICINGA_PUSHER_CA="-k"
```

The `ICINGA_PUSHER_CREDS` contain the Icinga API user credentials, either a
simple `"-u user:password"` combination or it could be `"--cert
/etc/ssl/yourclient.crt --key /etc/ssl/yourclient.key"`.

The `ICINGA_PUSHER_URL` is the base URL of your Icinga setup, for the
API port which is usually 5665.

The `ICINGA_PUSHER_CA` is `"--cacert /etc/ssl/icingaca.crt"` or `"-k"` to
not use any CA verification (not recommended!).

The `ICINGA_PUSHER_CA` and `ICINGA_PUSHER_CREDS` variables are passed to
[curl](https://curl.haxx.se/).

The script would typically be invoked from a cron job, such as the
following `/etc/cron.d/icinga-pusher` file:

~~~~
30 * * * * munin /usr/local/bin/icinga-pusher `hostname -f` passive-apt /usr/lib/nagios/plugins/check_apt
40 * * * * munin /usr/local/bin/icinga-pusher `hostname -f` passive-disk "/usr/lib/nagios/plugins/check_disk -w 20\% -c 5\% -X tmpfs -X devtmpfs"
~~~~

The Icinga server needs to have the REST API enabled, through the
following configuration (replace password to match the password
above):

~~~~
icinga# cat > /etc/icinga2/conf.d/api-users.conf
object ApiUser "pusher" {
  password = "blahonga"
  permissions = [ "actions/process-check-result" ]
}
^D
icinga# icinga2 feature enable api && systemctl reload icinga2
~~~~

The Icinga service definitions could be in
`/etc/icinga2/conf.d/services.conf` as follows.

~~~~
apply Service "passive-disk" {
  import "generic-service"
  check_command = "passive"
  check_interval = 2h
  assign where host.vars.os == "Debian"
}
apply Service "passive-apt" {
  import "generic-service"
  check_command = "passive"
  check_interval = 2h
  assign where host.vars.os == "Debian"
}
~~~~

Happy Hacking!
